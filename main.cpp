#include <iostream>

using namespace std;

void display(int table[10]);

void change(int &a, int &b);

int main() {
    int table[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int a = 10;
    int b = 20;
    cout << "table[1]: " << *table << endl;
    cout << "table[3]: " << *(table + 2) << endl << endl;
    display(table);
    cout << endl;
    cout << "Przed zamiana: " << endl;
    cout << "a: " << a << endl;
    cout << "b: " << b << endl << endl;
    cout << "Po zamianie: " << endl;
    change(a, b);
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    return 0;
}

void display(int table[10]) {
    for (int i = 0; i < 10; i++) {
        cout << "table[" << i + 1 << "]: " << table[i] << endl;
    }
}

void change(int &a, int &b) {
    int c = a;
    a = b;
    b = c;
}
